import sys


def readfile(filename):
   file = open(filename, 'r')
   lines = file.readlines()
   file.close()
   return lines

def writefile(filename, lines):
   file = open(filename, 'w') 
   file.writelines(lines) 
   file.close() 

def removeLines(lines):
   newlines = []
   remove = False
   for line in lines:
      if "# START REMOVE" in line:
         remove = True
      elif not remove:
         newlines.append(line)
        
      if "# END REMOVE" in line:
         remove = False
   return newlines

  
if __name__=="__main__": 
   inputfile = sys.argv[1]
   outputfile = sys.argv[2]
   print(inputfile,outputfile)
   lines = readfile(inputfile)
   nlines = removeLines(lines)
   writefile(outputfile,nlines)

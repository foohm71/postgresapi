# postgresAPI

## What is this?
A simple postgres API packaged as a docker container to access a postgres DB

How to run the docker container:

```docker run -e POSTGRES_HOST=hh-pgsql-public.ebi.ac.uk -e POSTGRES_PORT=5432 -e POSTGRES_USERNAME=reader -e POGRES_PASSWORD=NWDMCE5xdipIjRrp -e POSTGRES_DB=pfmegrnargs -it -p 5000:33 foohm71/postgresapi:main```

This DB: `hh-pgsql-public.ebi.ac.uk` is a publicly available DB you can use to test. Of course you should use your own postgres DB creds. 

Once you run the `docker run` command above, it will be listening to `http://localhost:5000`

## How to call the API

The `test.sh` shows you how to make the call to the API. It takes in a payload `test.json`

`test.sh` looks like this:

```
curl -X POST "http://localhost:5000/query" -d @test.json --header "Content-Type: application/json"
```

`test.json` looks like this:
```
{"column": "seq_short", "table": "rna", "where_key": "upi", "where_value": "URS00004AB3AD"}
```

This translates to:
```
SELECT seq_short 
FROM rna
WHERE upi = URS00004AB3AD
```

## What's with all the other files

This docker image was built using a Python Notebook - `PostgresAPI.ipynb`. It uses the psycopg2 library to access postgres and Flask to build the API. It is then converted to python using `nbconvert` and packaged into a docker image as described in `Dockerfile`. To see how the image is built see `.gitlab-ci.yml`.